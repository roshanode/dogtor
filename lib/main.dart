import 'package:dogtor/View/AdminScreens/adminMainScreen.dart';
import 'package:dogtor/View/AuthScreens/loginScreen.dart';
import 'package:dogtor/View/AuthScreens/splash.dart';
import 'package:dogtor/View/UserScreens/userMain.dart';
import 'package:dogtor/View/AuthScreens/signUpScreen.dart';
import 'package:dogtor/View/AuthScreens/emailVerificationPending.dart';
import 'package:dogtor/View/DoctorScreens/doctorMainScreen.dart';
import 'package:dogtor/View/DoctorScreens/pendingVerification.dart';
import 'package:dogtor/View/DoctorScreens/profileSetup.dart';
import 'package:dogtor/View/otherScreens/BreedsScreens/allBreeds.dart';
import 'package:dogtor/View/otherScreens/DiseaseScreens/allDiseases.dart';
import 'package:dogtor/View/otherScreens/PostScreens/postDetails.dart';
import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MyApp(),
      ),
    );

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Splash(),
      routes: <String, WidgetBuilder>{
        '/LoginScreen': (BuildContext context) => LoginScreen(),
        '/EmailVerificationPending': (BuildContext context) =>
            EmailVerificationPending(),
        '/SignUpScreen': (BuildContext context) => SignUpScreen(),
        '/Splash': (BuildContext context) => Splash(),
        '/UserMainScreen': (BuildContext context) => UserMainScreen(),
        '/DoctorMainScreen': (BuildContext context) => DoctorMainScreen(),
        '/AdminMainScreen': (BuildContext context) => AdminMainScreen(),
        '/ProfileSetup': (BuildContext context) => ProfileSetup(),
        '/PostDetail': (BuildContext context) => PostDetail(),
        '/AllBreeds': (BuildContext context) => AllBreeds(),
        '/AllDiseases': (BuildContext context) => AllDiseases(),
        '/PendingVerification': (BuildContext context) => PendingVerification(),
      },
    );
  }
}
