import 'package:dogtor/View/customComponents/customComtainerAdmin.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MoreSettings extends StatefulWidget {
  @override
  _MoreSettingsState createState() => _MoreSettingsState();
}

class _MoreSettingsState extends State<MoreSettings> {
  String userEmail;

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.currentUser().then((firebaseUser) async {
      if (firebaseUser != null) {
        setState(() {
          userEmail = firebaseUser.email;
          print("Logged in user ID:- " + userEmail);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
          ),
          centerTitle: true,
          title: Text(
            "More Settings ",
            style: GoogleFonts.firaCode(
                color: Color(0xff203152), fontWeight: FontWeight.bold),
          ),
          actions: [],
        ),
        backgroundColor: Colors.black12,
        body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          duration: Duration(milliseconds: 800),
          content: Text("Tap back again to exit"),
        ),
        child:Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text("Hello, Admin 😉",
                            style: GoogleFonts.firaCode(
                                fontSize: 20, fontWeight: FontWeight.w500)),
                        (userEmail != null)
                            ? Text(userEmail,
                                style: GoogleFonts.firaCode(
                                    fontSize: 16, fontWeight: FontWeight.w500))
                            : CircularProgressIndicator(),
                      ],
                    )),
                CustomContainer(
                  section: "Post Section",
                  icon: "blog",
                  view: "View All Posts",
                  add: "Add New Post",
                  route: "post",
                ),
                CustomContainer(
                  icon: "dogs",
                  section: "Dog Breeds Section",
                  view: "View All Dog Breeds",
                  add: "Add New Dog Breeds",
                  route: "breed",
                ),
                CustomContainer(
                    icon: "virus",
                    section: "Dog Diseases Section",
                    view: "View All Dog Diseases",
                    add: "Add New Dog Diseases",
                    route: "disease"),
                CustomContainer(
                  icon: "vaccine",
                  section: "Dog Vaccination Section",
                  view: "View All Dog Vaccination",
                  add: "Add New Dog vaccination",
                  route: "vaccine",
                ),
              ],
            ),
          ),
        ),),);
  }
}
