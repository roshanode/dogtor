import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dogtor/View/UserScreens/userDetails.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class VerificationPending extends StatefulWidget {
  @override
  _VerificationPendingState createState() => _VerificationPendingState();
}

class _VerificationPendingState extends State<VerificationPending> {
  String userType = "Expert";
  Widget _buildListItemPosts(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => UserDetail(
                      profileImage: document['photoUrl'],
                      fullName: document['fullName'],
                      email: document['email'],
                      userId: document['id'],
                      contact: document['contact'],
                      userType: document['userType'],
                      verificationDocument: document['verificationDocument'],
                      address: document['address'],
                      isVerified: document["isVerified"],
                      id: document.documentID,
                      collectionName: collectionName,
                    )),
          );
        },
        child: Container(
          margin: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 10),
          width: MediaQuery.of(context).size.width / 1.6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [
                new BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.5),
                  blurRadius: 20.0,
                ),
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                    imageUrl: document['photoUrl'],
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 4,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height / 7,
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      document['fullName'],
                      overflow: TextOverflow.fade,
                      style: GoogleFonts.firaCode(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      document['email'],
                      overflow: TextOverflow.fade,
                      style: GoogleFonts.firaCode(color: Colors.black),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
          ),
          title: Text(
            "Pending Verifications",
            style: GoogleFonts.firaCode(
                color: Color(0xff203152), fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          duration: Duration(milliseconds: 800),
          content: Text("Tap back again to exit"),
        ),
        child:Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.black12,
          child: Column(
            children: <Widget>[
              Expanded(
                child: StreamBuilder(
                  stream: Firestore.instance.collection('users').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    } else {
                      var userPastEvents = [];
                      for (int i = 0; i < snapshot.data.documents.length; i++) {
                        if (snapshot.data.documents[i]["userType"] ==
                            "Doctor") {
                          if (snapshot.data.documents[i]["isVerified"] ==
                              "Not Verified") {
                            userPastEvents.add(snapshot.data.documents[i]);
                          }
                        }
                      }
                      if (userPastEvents != null) {
                        return ListView.builder(
                          // itemExtent: 200,
                          scrollDirection: Axis.vertical,
                          itemCount: userPastEvents.length,
                          itemBuilder: (context, index) => _buildListItemPosts(
                              context, userPastEvents[index], 'users'),
                        );
                      } else {
                        return Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black12,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset('lib/Assets/puzzled.gif'),
                              Text(
                                "No new verification Request",
                                style: GoogleFonts.firaCode(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                  },
                ),
              ),
            ],
          ),
        ),),);
  }
}
