import 'package:dogtor/View/customComponents/offline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_fonts/google_fonts.dart';

class AddVaccination extends StatefulWidget {
  @override
  _AddVaccinationState createState() => _AddVaccinationState();
}

class _AddVaccinationState extends State<AddVaccination> {
  String _errorMessage;
  bool _isLoading = false;

  final databaseReference = Firestore.instance;
  TextEditingController booster = TextEditingController();
  TextEditingController comments = TextEditingController();
  TextEditingController initialAdult = TextEditingController();
  TextEditingController initialPuppy = TextEditingController();
  TextEditingController vaccinename = TextEditingController();

  void postData() async {
    if (booster.text.isNotEmpty &&
        initialAdult.text.isNotEmpty &&
        initialPuppy.text.isNotEmpty &&
        vaccinename.text.isNotEmpty) {
      setState(() {
        _isLoading = true;
      });

      try {
        await Firestore.instance.collection('Vaccination').document().setData({
          'Booster Recommendation': booster.text.trim(),
          'Comments': comments.text.trim(),
          'Initial Adult Dog Vaccination (over 16 weeks)':
              initialAdult.text.trim(),
          'Initial Puppy Vaccination (at or under 16 weeks)':
              initialPuppy.text.trim(),
          'Vaccination Name': vaccinename.text.trim(),
        }).then((_) {
          setState(() {
            booster.clear();
            initialPuppy.clear();
            initialAdult.clear();
            comments.clear();
            vaccinename.clear();
            _isLoading = false;
            Fluttertoast.showToast(msg: 'Added Successfully');
          });
        });
      } catch (e) {
        _errorMessage = e;
        Fluttertoast.showToast(msg: _errorMessage);
      }
    } else {
      Fluttertoast.showToast(msg: 'Please fill all fields');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 30,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        centerTitle: true,
        title: Text(
          'Add Dog Vaccination Data',
          style: GoogleFonts.firaCode(
              color: Color(0xff203152), fontWeight: FontWeight.bold),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 15),
                      TextFormField(
                        controller: vaccinename,
                        decoration: InputDecoration(
                          labelText: "Vaccination Name",
                          hintText: "Vaccination Name",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: initialPuppy,
                        decoration: InputDecoration(
                          labelText:
                              "Initial Puppy Vaccination (at or under 16 weeks)",
                          hintText:
                              "Initial Puppy Vaccination (at or under 16 weeks).",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: initialAdult,
                        decoration: InputDecoration(
                          labelText:
                              "Initial Adult Dog Vaccination (over 16 weeks)",
                          hintText:
                              "Initial Adult Dog Vaccination (over 16 weeks)",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: booster,
                        decoration: InputDecoration(
                          labelText: "Booster Recommendation",
                          hintText: "Booster Recommendation",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: comments,
                        maxLines: 10,
                        decoration: InputDecoration(
                          labelText: "Comments",
                          hintText: "Comments",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      OfflineBuilder(
                        connectivityBuilder: (
                          BuildContext context,
                          ConnectivityResult connectivity,
                          Widget child,
                        ) {
                          bool connected =
                              connectivity != ConnectivityResult.none;
                          return connected
                              ? InkWell(
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());

                                    postData();
                                  },
                                  child: Container(
                                      height: 50,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.orange,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Center(
                                          child: Text(
                                        'Add',
                                        style: GoogleFonts.firaCode(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20.0),
                                      ))),
                                )
                              : Offline();
                        },
                        child: Container(),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            _isLoading
                ? Positioned.fill(
                    child: Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.black.withOpacity(0.5),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(
                              backgroundColor: Colors.green,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Uploading! Please Wait :)",
                                style: GoogleFonts.firaCode(
                                    color: Colors.white, fontSize: 20),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
