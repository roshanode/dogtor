import 'package:dogtor/View/customComponents/offline.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AddDogBreed extends StatefulWidget {
  @override
  _AddDogBreedState createState() => _AddDogBreedState();
}

class _AddDogBreedState extends State<AddDogBreed> {
  List<Asset> images = List<Asset>();
  List<String> imageUrls = <String>[];
  String _errorMessage;
  bool _isLoading = false;

  final databaseReference = Firestore.instance;
  TextEditingController dogBreed = TextEditingController();
  TextEditingController dogHeight = TextEditingController();
  TextEditingController dogWeight = TextEditingController();
  TextEditingController dogLifeSpan = TextEditingController();
  TextEditingController dogDescription = TextEditingController();

  Future<void> getImage() async {
    List<Asset> resultList = List<Asset>();

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 2,
        enableCamera: true,

        selectedAssets: images,

        // ),
      );
    } on Exception catch (e) {
      _errorMessage = e.toString();
    }

    if (!mounted) return;
    setState(() {
      images = resultList;
      _errorMessage = _errorMessage;
    });
  }

  void uploadImages() async {
    if (images.isNotEmpty &&
        dogBreed.text.isNotEmpty &&
        dogDescription.text.isNotEmpty &&
        dogHeight.text.isNotEmpty &&
        dogLifeSpan.text.isNotEmpty &&
        dogWeight.text.isNotEmpty) {
      setState(() {
        _isLoading = true;
      });
      for (var imageFile in images) {
        await postImage(imageFile).then((downloadUrl) {
          imageUrls.add(downloadUrl.toString());
          if (imageUrls.length == images.length) {
            Firestore.instance.collection('Dogs Breeds').document().setData({
              'Breed': dogBreed.text.trim(),
              'Description': dogDescription.text.trim(),
              'Pictures': imageUrls,
              'Height': dogHeight.text.trim(),
              'Weight': dogWeight.text.trim(),
              'Life Span': dogLifeSpan.text.trim(),
            }).then((_) {
              setState(() {
                images = [];
                imageUrls = [];
                dogBreed.clear();
                dogDescription.clear();
                dogHeight.clear();
                dogLifeSpan.clear();
                dogWeight.clear();
                _isLoading = false;
                Fluttertoast.showToast(msg: 'Added Successfully');
              });
            });
          }
        }).catchError((err) {
          setState(() {
            _isLoading = false;
          });
          print(err);
        });
      }
    } else {
      Fluttertoast.showToast(msg: 'Please fill all fields');
    }
  }

  Future<dynamic> postImage(Asset imageFile) async {
    String fileName = DateTime.now().millisecondsSinceEpoch.toString();
    StorageReference reference =
        FirebaseStorage.instance.ref().child('Dog Breeds').child(fileName);
    StorageUploadTask uploadTask =
        reference.putData((await imageFile.getByteData()).buffer.asUint8List());
    StorageTaskSnapshot storageTaskSnapshot = await uploadTask.onComplete;
    print(storageTaskSnapshot.ref.getDownloadURL());
    return storageTaskSnapshot.ref.getDownloadURL();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 30,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        centerTitle: true,
        title: Text(
          'Add Dog Breeds Data',
          style: GoogleFonts.firaCode(
              color: Color(0xff203152), fontWeight: FontWeight.bold),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 15),
                      TextFormField(
                        controller: dogBreed,
                        decoration: InputDecoration(
                          labelText: "Dog Breed",
                          hintText: "Dog Breed",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: dogHeight,
                        decoration: InputDecoration(
                          labelText: "Dog Height in ft.",
                          hintText: "Dog Height in ft.",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: dogWeight,
                        decoration: InputDecoration(
                          labelText: "Dog Weight in KG",
                          hintText: "Dog Weight in KG",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: dogLifeSpan,
                        decoration: InputDecoration(
                          labelText: "Dog Life Span in Years",
                          hintText: "Dog Life Span in Years",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: dogDescription,
                        maxLines: 10,
                        decoration: InputDecoration(
                          labelText: "Dog Description",
                          hintText: "Dog Description",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      InkWell(
                        onTap: () {
                          getImage();
                        },
                        child: Container(
                          alignment: Alignment.center,
                          height: MediaQuery.of(context).size.height / 2.6,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              border: Border.all(width: 1, color: Colors.grey),
                              borderRadius: BorderRadius.circular(5)),
                          child: (images.isNotEmpty)
                              ? GridView.count(
                                  crossAxisCount: 2,
                                  children:
                                      List.generate(images.length, (index) {
                                    Asset asset = images[index];

                                    return Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Container(
                                        // height: 50,
                                        // width: 50,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15)),
                                          child: AssetThumb(
                                            asset: asset,
                                            width: 300,
                                            height: 300,
                                          ),
                                        ),
                                      ),
                                    );
                                  }),
                                )
                              : Container(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      "Choose or take a picture",
                                      style: GoogleFonts.firaCode(),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      OfflineBuilder(
                        connectivityBuilder: (
                          BuildContext context,
                          ConnectivityResult connectivity,
                          Widget child,
                        ) {
                          bool connected =
                              connectivity != ConnectivityResult.none;
                          return connected
                              ? InkWell(
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());

                                    uploadImages();
                                  },
                                  child: Container(
                                      height: 50,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.orange,
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Center(
                                          child: Text(
                                        'Add',
                                        style: GoogleFonts.firaCode(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20.0),
                                      ))),
                                )
                              : Offline();
                        },
                        child: Container(),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            _isLoading
                ? Positioned.fill(
                    child: Container(
                      height: 100,
                      width: 100,
                      color: Colors.black.withOpacity(0.5),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(
                              backgroundColor: Colors.green,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Uploading! Please Wait :)",
                                style: GoogleFonts.firaCode(
                                    color: Colors.white, fontSize: 20),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
