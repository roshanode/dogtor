import 'package:dogtor/View/AdminScreens/addDisease.dart';
import 'package:dogtor/View/AdminScreens/addDogBreed.dart';
import 'package:dogtor/View/AdminScreens/addVaccination.dart';
import 'package:dogtor/View/otherScreens/BreedsScreens/allBreeds.dart';
import 'package:dogtor/View/otherScreens/DiseaseScreens/allDiseases.dart';
import 'package:dogtor/View/otherScreens/PostScreens/feeds.dart';
import 'package:dogtor/View/otherScreens/PostScreens/newPost.dart';
import 'package:dogtor/View/otherScreens/VaccineScreens/vaccine.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomContainer extends StatefulWidget {
  final String icon;
  final String section;
  final String view;
  final String add;
  final String route;
  CustomContainer(
      {Key key, this.icon, this.add, this.section, this.view, this.route})
      : super(key: key);
  @override
  _CustomContainerState createState() => _CustomContainerState();
}

class _CustomContainerState extends State<CustomContainer> {
  @override
  Widget build(BuildContext context) {
    String ico = widget.icon;
    String rou = widget.route;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Color(0xffff2193b0), Color(0xffff6dd5ed)]),
          border: Border.all(width: 1, color: Colors.black.withOpacity(.5)),
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10), topLeft: Radius.circular(10)),
              gradient: LinearGradient(
                  colors: [Color(0xffffD3CCE3), Color(0xffffE9E4F0)]),
            ),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "lib/Assets/$ico.png",
                  height: 40,
                  width: 40,
                ),
                Text(
                  widget.section,
                  style: GoogleFonts.firaCode(
                      fontSize: 16, fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
          Divider(
            height: 1,
            color: Colors.black,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: InkWell(
                    onTap: () {
                      if (rou == "post") {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Feeds()));
                      } else if (rou == "breed") {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AllBreeds()));
                      } else if (rou == "disease") {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AllDiseases()));
                      } else {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Vaccine()));
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(right: 10),
                      height: 150,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Color(0xffffD3CCE3),
                            Color(0xffffE9E4F0)
                          ]),
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(width: .5)),
                      child: Center(
                        child: Text(
                          widget.view,
                          style: GoogleFonts.firaCode(
                              fontSize: 14, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                ),
                Flexible(
                  child: InkWell(
                    onTap: () {
                      if (rou == "post") {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => NewPost()));
                      } else if (rou == "breed") {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddDogBreed()));
                      } else if (rou == "disease") {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddDisease()));
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddVaccination()));
                      }
                    },
                    child: Container(
                      height: 150,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Color(0xffffD3CCE3),
                            Color(0xffffE9E4F0)
                          ]),
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(width: .5)),
                      child: Center(
                        child: Text(
                          widget.add,
                          style: GoogleFonts.firaCode(
                              fontSize: 14, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
