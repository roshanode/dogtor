import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class PendingVerification extends StatefulWidget {
  @override
  _PendingVerificationState createState() => _PendingVerificationState();
}

class _PendingVerificationState extends State<PendingVerification> {
  void signOut() async {
    try {
      await FirebaseAuth.instance.signOut();
      Navigator.of(context).pushReplacementNamed('/LoginScreen');
    } catch (e) {
      print(e);
    }
  }

  void _exitAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                    colors: [Color(0xffffff7e5f), Color(0xfffffeb47b)])),
            height: 100,
            width: 100,
            child: Column(
              children: [
                Expanded(
                  child: Text(
                    "Are You Sure ?",
                    style: GoogleFonts.firaCode(
                        fontSize: 20, fontWeight: FontWeight.w800),
                  ),
                ),
                Expanded(
                  child: Text(
                    "Want to log-out",
                    style: GoogleFonts.firaCode(
                        fontSize: 16, fontWeight: FontWeight.w500),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        IconButton(
                          icon: FaIcon(FontAwesomeIcons.timesCircle),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Text("No".toUpperCase())
                      ],
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: FaIcon(FontAwesomeIcons.signOutAlt),
                          onPressed: () {
                            Navigator.of(context).pop();
                            signOut();
                          },
                        ),
                        Text("Yes".toUpperCase())
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Color(0xffff757F9A), Color(0xffffD7DDE8)])),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "We are extremely sorry !",
                        style: GoogleFonts.firaCode(
                            fontSize: 20, fontWeight: FontWeight.w600),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: Image.asset(
                          'lib/Assets/disappointed.gif',
                          height: 60,
                          width: 60,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 2,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl:
                            'https://media2.giphy.com/media/d7fTn7iSd2ivS/200.webp?cid=ecf05e47d38c89e0ab7488d7d470ead9b24b0b3e6a38e7b4&rid=200.webp'),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      Text(
                        "Your account has not been verified yet!",
                        style: GoogleFonts.firaCode(
                            fontSize: 20, fontWeight: FontWeight.w600),
                      ),
                      Text(
                        "Please wait until the admin verifies your account!",
                        style: GoogleFonts.firaCode(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: RaisedButton(
                    splashColor: Colors.blue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        side: BorderSide(color: Colors.red)),
                    onPressed: () => _exitAlert(),
                    elevation: 20,
                    color: Colors.white24,
                    child: Text(
                      "Log Out".toUpperCase(),
                      style: GoogleFonts.firaCode(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
