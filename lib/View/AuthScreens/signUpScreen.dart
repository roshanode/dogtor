import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();

  String email;
  String password;
  String fullName;
  String contact;
  String address;
  String _errorMessage;
  String userType = "User";
  bool _isLoading = false;
  bool _obscureText = true;
  String userId;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
// Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  // Check if form is valid before perform login
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<String> signUp(String email, String password) async {
    FirebaseUser user = (await _firebaseAuth.createUserWithEmailAndPassword(
            email: email, password: password))
        .user;
    userId = user.uid;
    return user.uid;
  }

  // Perform login or signup
  void _validateAndSubmit() async {
    if (_validateAndSave()) {
      setState(() {
        _errorMessage = "";
        _isLoading = true;
      });

      try {
        await signUp(email, password);
        FirebaseUser user = await _firebaseAuth.currentUser();
        await user.sendEmailVerification();
        await Firestore.instance.collection('users').document(userId).setData({
          'fullName': fullName,
          'email': email,
          'contact': contact,
          'address': address,
          'photoUrl':
              'https://toppng.com/public/uploads/preview/user-account-management-logo-user-icon-11562867145a56rus2zwu.png',
          'userType': userType,
          'id': userId,
          'isVerified': 'Not Verified',
          'isFirstTime': 'yes',
          'chattingWith': 'none',
          'pushToken': '',
          'verificationDocument': 'null'
        });
        await FirebaseAuth.instance.signOut();
        await Fluttertoast.showToast(
            msg:
                "Sign Up Success\n We have sent you a mail to verify your email,\n Please verify ypur email before logging in",
            toastLength: Toast.LENGTH_SHORT,
            backgroundColor: Colors.blue,
            textColor: Colors.white);
        Navigator.of(context).pushReplacementNamed('/LoginScreen');
        print('Signed up user: $userId');
      } catch (e) {
        setState(() {
          _isLoading = false;

          _errorMessage = e.message;
          Fluttertoast.showToast(
              msg: _errorMessage,
              toastLength: Toast.LENGTH_SHORT,
              backgroundColor: Colors.red,
              textColor: Colors.white);
        });
      }
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          duration: Duration(milliseconds: 800),
          content: Text("Tap back again to exit"),
        ),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Stack(
            children: <Widget>[
              Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 2.5,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [Color(0xFFf45d27), Color(0xFFf5851f)],
                            ),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(90))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Spacer(),
                            Align(
                                alignment: Alignment.center,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: Image.asset(
                                    'lib/Assets/dogtor.png',
                                    height: 150,
                                    width: 150,
                                  ),
                                )),
                            Spacer(),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 32, right: 32),
                                child: Text(
                                  'Register',
                                  style: GoogleFonts.firaCode(
                                      color: Colors.white,
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: TextFormField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            prefixIcon: Icon(
                              Icons.person,
                              color: Colors.grey,
                            ),
                            labelText: 'Full Name',
                            hintText: 'Full Name',
                            labelStyle: GoogleFonts.firaCode(),
                            hintStyle: GoogleFonts.firaCode(),
                          ),
                          validator: validateName,
                          onSaved: (value) => fullName = value.trim(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            prefixIcon: Icon(
                              Icons.email,
                              color: Colors.grey,
                            ),
                            labelText: 'Email',
                            hintText: 'Email',
                            labelStyle: GoogleFonts.firaCode(),
                            hintStyle: GoogleFonts.firaCode(),
                          ),
                          validator: validateEmail,
                          onSaved: (value) => email = value.trim(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: Text(
                          "* Please use the valid Gmail ID. You need to verify it later.",
                          style: GoogleFonts.firaCode(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontStyle: FontStyle.italic),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: TextFormField(
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              prefixIcon: Icon(
                                Icons.vpn_key,
                                color: Colors.grey,
                              ),
                              suffixIcon: IconButton(
                                  icon: FaIcon(
                                    _obscureText
                                        ? FontAwesomeIcons.eyeSlash
                                        : FontAwesomeIcons.eye,
                                    color: Colors.deepOrange,
                                  ),
                                  onPressed: _toggle),
                              labelStyle: GoogleFonts.firaCode(),
                              hintStyle: GoogleFonts.firaCode(),
                              hintText: 'Password',
                              labelText: 'Password'),
                          obscureText: _obscureText,
                          validator: validatePassword,
                          onSaved: (value) => password = value.trim(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            prefixIcon: Icon(
                              Icons.phone,
                              color: Colors.grey,
                            ),
                            labelText: 'Contact',
                            hintText: 'Contact',
                            labelStyle: GoogleFonts.firaCode(),
                            hintStyle: GoogleFonts.firaCode(),
                          ),
                          validator: validateMobile,
                          onSaved: (value) => contact = value.trim(),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.2,
                        margin: EdgeInsets.only(top: 30),
                        child: TextFormField(
                          maxLines: 1,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              labelText: 'Address',
                              hintText: 'Address',
                              labelStyle: GoogleFonts.firaCode(),
                              hintStyle: GoogleFonts.firaCode(),
                              prefixIcon: Icon(
                                Icons.person,
                                color: Colors.grey,
                              )),
                          validator: validateName,
                          onSaved: (value) => address = value.trim(),
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: Text(
                              "Choose Account Type",
                              style: GoogleFonts.firaCode(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  this.setState(() {
                                    this.userType = "User";
                                    print(userType);
                                  });
                                },
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(width: 0.5),
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: this.userType == "User"
                                            ? Colors.orange
                                            : Colors.white,
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 15),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Image.asset(
                                            "lib/Assets/dog.png",
                                            height: 30.0,
                                            width: 30.0,
                                            color: this.userType == "User"
                                                ? Colors.white
                                                : Colors.orange,
                                          ),
                                          Text(
                                            "User",
                                            style: GoogleFonts.firaCode(
                                                color: this.userType == "User"
                                                    ? Colors.white
                                                    : Colors.orange),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  this.setState(() {
                                    this.userType = "Doctor";
                                    print(userType);
                                  });
                                },
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(width: 0.5),
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: this.userType == "Doctor"
                                            ? Colors.orange
                                            : Colors.white,
                                      ),
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 15),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Image.asset(
                                            "lib/Assets/veterinary.png",
                                            height: 30.0,
                                            width: 30.0,
                                            color: this.userType == "Doctor"
                                                ? Colors.white
                                                : Colors.orange,
                                          ),
                                          Text(
                                            "Doctor",
                                            style: GoogleFonts.firaCode(
                                                color: this.userType == "Doctor"
                                                    ? Colors.white
                                                    : Colors.orange),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              left: 20,
                              right: 20,
                              top: 20,
                              bottom: 20,
                            ),
                            child: InkWell(
                              onTap: () {
                                _validateAndSubmit();
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                              },
                              child: Container(
                                height: 45,
                                width: MediaQuery.of(context).size.width / 1.2,
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: [
                                        Color(0xFFf45d27),
                                        Color(0xFFf5851f)
                                      ],
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50))),
                                child: Center(
                                  child: Text('Create account',
                                      style: GoogleFonts.firaCode(
                                          fontSize: 20.0, color: Colors.white)),
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.of(context)
                                  .pushReplacementNamed('/LoginScreen');
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 20),
                              alignment: Alignment.bottomCenter,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Already have an account ?',
                                    style: GoogleFonts.firaCode(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Login',
                                    style: GoogleFonts.firaCode(
                                        color: Color(0xfff79c4f),
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              _isLoading
                  ? Positioned.fill(
                      child: Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.black.withOpacity(0.5),
                        child: Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.red,
                          ),
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }

  //validators
  //Name validator
  String validateName(String value) {
    if (value.length < 5)
      return 'Name must be more than 2 charater';
    else
      return null;
  }

  //validator for contact number
  String validateMobile(String value) {
// Nepal's Mobile number are of 10 digit only
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  //validator for email
  String validateEmail(String value) {
    Pattern pattern = r'^[a-z0-9](\.?[a-z0-9]){5,}@gmail\.com$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  //validator for password
  String validatePassword(String value) {
    if (value.length < 8)
      return 'Password must be 8 character long';
    else
      return null;
  }
}
