import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class EmailVerificationPending extends StatefulWidget {
  @override
  _EmailVerificationPendingState createState() =>
      _EmailVerificationPendingState();
}

class _EmailVerificationPendingState extends State<EmailVerificationPending> {
  String email;
  void sendVerificationEmail() async {
    await FirebaseAuth.instance.currentUser().then((user) {
      email = user.email;
      user.sendEmailVerification();
    });
  }

  void signOut() async {
    try {
      await FirebaseAuth.instance.signOut();
      Navigator.of(context).pushReplacementNamed('/LoginScreen');
    } catch (e) {
      print(e);
    }
  }

  void _exitAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                    colors: [Color(0xffffff7e5f), Color(0xfffffeb47b)])),
            height: 100,
            width: 100,
            child: Column(
              children: [
                Flexible(
                  child: Text("Are You Sure ?",
                      style: GoogleFonts.firaCode(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.w800)),
                ),
                Flexible(
                  child: Text(
                    "Want to log-out",
                    style: GoogleFonts.firaCode(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        IconButton(
                          icon: FaIcon(
                            FontAwesomeIcons.timesCircle,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Text(
                          "No".toUpperCase(),
                          style: GoogleFonts.firaCode(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: FaIcon(
                            FontAwesomeIcons.signOutAlt,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            signOut();
                            Navigator.of(context).pop();
                          },
                        ),
                        Text(
                          "Yes".toUpperCase(),
                          style: GoogleFonts.firaCode(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
          ),
          title: Text(
            "Email Verification Pending",
            style: GoogleFonts.firaCode(),
          ),
          actions: <Widget>[
            Center(
                child: Text(
              "Log Out".toUpperCase(),
              style: GoogleFonts.firaCode(),
            )),
            IconButton(
              onPressed: () => _exitAlert(),
              icon: Icon(Icons.exit_to_app),
            ),
          ],
        ),
        body: DoubleBackToCloseApp(
          snackBar: const SnackBar(
            duration: Duration(milliseconds: 800),
            content: Text("Tap back again to exit"),
          ),
          child: SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 1.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: CachedNetworkImage(
                        imageUrl:
                            "https://media.giphy.com/media/GDnomdqpSHlIs/giphy.gif"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(
                      "Sorry, It seems your email is not verified yet",
                      style: GoogleFonts.firaCode(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: ArgonTimerButton(
                      height: 50,
                      width: MediaQuery.of(context).size.width * 0.45,
                      onTap: (startTimer, btnState) {
                        if (btnState == ButtonState.Idle) {
                          startTimer(5);
                          sendVerificationEmail();
                        }
                      },
                      child: Text(
                        "Resend Verification Email",
                        style: GoogleFonts.firaCode(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w700),
                      ),
                      loader: (timeLeft) {
                        return Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(50)),
                          margin: EdgeInsets.all(5),
                          alignment: Alignment.center,
                          width: 40,
                          height: 40,
                          child: Text(
                            "$timeLeft",
                            style: GoogleFonts.firaCode(
                                fontSize: 20, fontWeight: FontWeight.w700),
                          ),
                        );
                      },
                      borderRadius: 5.0,
                      color: Colors.red,
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
