import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formKeyReset = GlobalKey<FormState>();
  bool _obscureText = true;
  String email;
  String password;
  String emailReset;
  String _errorMessage;
  String loggedInUserType;
  String isFirstTime;
  String isVerified;
  bool _isLoading = false;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  // Check if form is valid before perform login
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<String> signIn(String email, String password) async {
    print('===========>' + email);
    FirebaseUser user = (await _firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password))
        .user;
    if (user.isEmailVerified) {
      return user.uid;
    } else {
      return null;
    }
  }

  // Perform login or signup
  void _validateAndSubmit() async {
    if (_validateAndSave()) {
      setState(() {
        _errorMessage = "";
        _isLoading = true;
      });
      String userId = "";

      try {
        userId = await signIn(email, password);
        if (userId != null) {
          await Firestore.instance
              .collection('users')
              .document(userId)
              .get()
              .then((DocumentSnapshot ds) {
            setState(() {
              loggedInUserType = ds.data['userType'];
              isFirstTime = ds.data['isFirstTime'];
              isVerified = ds.data['isVerified'];
            });
            // use ds as a snapshot
            print(loggedInUserType);
          });
          print('Signed in: $userId');
          Fluttertoast.showToast(
              msg: "LoginSuccess",
              toastLength: Toast.LENGTH_SHORT,
              backgroundColor: Colors.blue,
              textColor: Colors.white);
        } else {
          Navigator.of(context)
              .pushReplacementNamed('/EmailVerificationPending');
        }
      } catch (e) {
        setState(() {
          _isLoading = false;

          _errorMessage = e.message;
          Fluttertoast.showToast(
              msg: _errorMessage,
              toastLength: Toast.LENGTH_SHORT,
              backgroundColor: Colors.red,
              textColor: Colors.white);
        });
      }
      setState(() {
        _isLoading = false;
      });
      try {
        if (userId.length > 0 && userId != null) {
          if (loggedInUserType == "Doctor") {
            if (isFirstTime == "yes") {
              Navigator.of(context).pushReplacementNamed('/ProfileSetup');
            } else {
              if (isVerified == "Verified") {
                Navigator.of(context).pushReplacementNamed('/DoctorMainScreen');
              } else {
                Navigator.of(context)
                    .pushReplacementNamed('/PendingVerification');
              }
            }
          } else if (loggedInUserType == "User") {
            Navigator.of(context).pushReplacementNamed('/UserMainScreen');
          } else if (loggedInUserType == "Admin") {
            Navigator.of(context).pushReplacementNamed('/AdminMainScreen');
          } else {
            Navigator.of(context).pushReplacementNamed('/LoginScreen');
          }
        }
      } catch (e) {
        print(e);
      }
    }
  }

//for password reset
  bool _validateAndSaveReset() {
    final form = _formKeyReset.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmitReset() async {
    if (_validateAndSaveReset()) {
      setState(() {
        _errorMessage = "";
      });

      try {
        sendPasswordResetMail(emailReset);

        setState(
          () {
            Fluttertoast.showToast(
                msg:
                    "Email sent to,$emailReset follow the link to reset your password",
                toastLength: Toast.LENGTH_LONG,
                backgroundColor: Colors.grey,
                textColor: Colors.white);

            Navigator.of(context).pushReplacementNamed('/LoginScreen');
          },
        );
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          Fluttertoast.showToast(
              msg: _errorMessage,
              toastLength: Toast.LENGTH_LONG,
              backgroundColor: Colors.red,
              textColor: Colors.white);
        });
      }
    }
  }

//send password reset mail
  Future<void> sendPasswordResetMail(String emailReset) async {
    print('===========>' + emailReset);
    await _firebaseAuth.sendPasswordResetEmail(email: emailReset);

    return null;
  }

//for password forget dialogue
  void _forgetPassword() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              width: MediaQuery.of(context).size.width / 1.2,
              height: 200,
              padding: EdgeInsets.only(top: 4, left: 16, right: 16, bottom: 4),
              child: Form(
                key: _formKeyReset,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "*Please enter your mail where the password reset email will be sent !",
                        style: GoogleFonts.firaCode(),
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.black, width: 2.0),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.red, width: 10.0),
                          ),
                          prefixIcon: Icon(
                            Icons.email,
                            color: Colors.grey,
                          ),
                          hintText: 'Email',
                          hintStyle: GoogleFonts.firaCode()),
                      validator: validateEmail,
                      onSaved: (value) => emailReset = value.trim(),
                    ),
                    InkWell(
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        _validateAndSubmitReset();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        height: 50,
                        width: MediaQuery.of(context).size.width / 1.2,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color(0xFFf45d27), Color(0xFFf5851f)],
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(50))),
                        child: Center(
                          child: Text(
                            'Send Password reset Email'.toUpperCase(),
                            style: GoogleFonts.firaCode(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          duration: Duration(milliseconds: 800),
          content: Text("Tap back again to exit"),
        ),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Stack(
            children: <Widget>[
              Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 2.5,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [Color(0xFFf45d27), Color(0xFFf5851f)],
                            ),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(90))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Spacer(),
                            Align(
                                alignment: Alignment.center,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: Image.asset(
                                    'lib/Assets/dogtor.png',
                                    height: 150,
                                    width: 150,
                                  ),
                                )),
                            Spacer(),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 32, right: 32),
                                  child: Text(
                                    'Login'.toUpperCase(),
                                    style: GoogleFonts.firaCode(
                                        color: Colors.white,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            prefixIcon: Icon(
                              Icons.email,
                              color: Colors.grey,
                            ),
                            labelText: 'Email',
                            hintText: 'Email',
                            labelStyle: GoogleFonts.firaCode(),
                            hintStyle: GoogleFonts.firaCode(),
                          ),
                          validator: validateEmail,
                          onSaved: (value) => email = value.trim(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: TextFormField(
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              prefixIcon: Icon(
                                Icons.vpn_key,
                                color: Colors.grey,
                              ),
                              suffixIcon: IconButton(
                                  icon: FaIcon(
                                    _obscureText
                                        ? FontAwesomeIcons.eyeSlash
                                        : FontAwesomeIcons.eye,
                                    color: Colors.deepOrange,
                                  ),
                                  onPressed: _toggle),
                              labelStyle: GoogleFonts.firaCode(),
                              hintStyle: GoogleFonts.firaCode(),
                              hintText: 'Password',
                              labelText: 'Password'),
                          obscureText: _obscureText,
                          validator: validatePassword,
                          onSaved: (value) => password = value.trim(),
                        ),
                      ),
                      Tooltip(
                        message: "You can now reset your passworsd",
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10, right: 50),
                            child: GestureDetector(
                              onTap: () {
                                _forgetPassword();
                                // Navigator.of(context).pushReplacementNamed('/forgetPassword');
                              },
                              child: Text(
                                'Forgot Password ?',
                                style: GoogleFonts.firaCode(
                                    color: Colors.grey,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          top: 20,
                          bottom: 20,
                        ),
                        child: InkWell(
                          onTap: () {
                            _validateAndSubmit();
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          },
                          child: Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width / 1.2,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color(0xFFf45d27),
                                    Color(0xFFf5851f)
                                  ],
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50))),
                            child: Center(
                                child: Text('Login',
                                    style: GoogleFonts.firaCode(
                                        fontSize: 20.0, color: Colors.white))),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context)
                              .pushReplacementNamed('/SignUpScreen');
                        },
                        child: Container(
                            margin: EdgeInsets.symmetric(vertical: 20),
                            alignment: Alignment.bottomCenter,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Don\'t have an account ?',
                                  style: GoogleFonts.firaCode(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  'Register',
                                  style: GoogleFonts.firaCode(
                                      color: Color(0xfff79c4f),
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )),
                      )
                    ],
                  ),
                ),
              ),
              _isLoading
                  ? Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.black.withOpacity(0.5),
                      child: Center(
                        child: CircularProgressIndicator(
                          strokeWidth: 1,
                          backgroundColor: Colors.redAccent,
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }

  //validator for email
  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  //validator for password
  String validatePassword(String value) {
    if (value.length < 8)
      return 'Password must be 8 character long';
    else
      return null;
  }
}
