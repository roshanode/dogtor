
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dogtor/View/AdminScreens/adminMainScreen.dart';
import 'package:dogtor/View/AuthScreens/loginScreen.dart';
import 'package:dogtor/View/UserScreens/userMain.dart';
import 'package:dogtor/View/AuthScreens/emailVerificationPending.dart';
import 'package:dogtor/View/DoctorScreens/doctorMainScreen.dart';
import 'package:dogtor/View/DoctorScreens/pendingVerification.dart';
import 'package:dogtor/View/DoctorScreens/profileSetup.dart';
import 'package:firebase_auth/firebase_auth.dart';
class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {

  String loggedInUserType;
  String userId;
  String isFirstTime;
  String url;
  bool emailVerified = false;
  String isVerified;
  @override
  void initState() {
    super.initState();


    Future.delayed(Duration(milliseconds: 3000)).then(
      (val) async {
        FirebaseAuth _auth = FirebaseAuth.instance;

        var user = await _auth.currentUser();

        if (user != null) {
          if (user.isEmailVerified) {
            if (mounted)
              setState(() {
                emailVerified = true;
                userId = user.uid;
              });
            await Firestore.instance
                .collection('users')
                .document(user.uid)
                .get()
                .then((DocumentSnapshot ds) {
              if (mounted)
                setState(() {
                  userId = user.uid;
                  isFirstTime = ds.data['isFirstTime'];
                  loggedInUserType = ds.data['userType'];
              
                  isVerified = ds.data['isVerified'];
                });
            });
          } else {
            if (mounted)
              setState(() {
                emailVerified = false;
                userId = user.uid;
              });
          }
        }

    
        
        if (userId != null) {
        if (emailVerified ) {
          if (loggedInUserType == "Doctor") {
            if (isFirstTime == "yes") {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => ProfileSetup()));
            } else {
              if (isVerified == "Verified") {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DoctorMainScreen()));
              } else {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PendingVerification()));
              }
            }
          } else if (loggedInUserType == "User") {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => UserMainScreen()));
          } else if (loggedInUserType == "Admin") {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => AdminMainScreen()));
          } else {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
          }
        } else if (!emailVerified) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => EmailVerificationPending()));
        } else {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => LoginScreen()));
        }
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginScreen()));
      }
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('lib/Assets/dogtor.png'),
            Text(
              "Please Wait..",
              style: GoogleFonts.firaCode(
                  fontSize: 18, fontWeight: FontWeight.w600),
            ),
            CircularProgressIndicator(
              backgroundColor: Colors.white,
              strokeWidth: 5,
            ),
          ],
        ),
      ),
    );
  }
}
