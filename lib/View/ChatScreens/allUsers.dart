import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dogtor/View/ChatScreens/chat.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AllUsers extends StatefulWidget {
  @override
  _AllUsersState createState() => _AllUsersState();
}

class _AllUsersState extends State<AllUsers> {
  bool isLoading = false;
  String currentUserId;
  String loggedInUserType;
  StreamSubscription<ConnectivityResult> internetconnection;
  bool isoffline = false;
  @override
  void initState() {
    super.initState();

    internetconnection = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      // whenevery connection status is changed.
      if (result == ConnectivityResult.none) {
        //there is no any connection
        setState(() {
          isoffline = true;
        });
      } else if (result == ConnectivityResult.mobile) {
        //connection is mobile data network
        setState(() {
          isoffline = false;
        });
      } else if (result == ConnectivityResult.wifi) {
        //connection is from wifi
        setState(() {
          isoffline = false;
        });
      }
    });
    FirebaseAuth.instance.currentUser().then(
      (firebaseUser) {
        if (firebaseUser != null) {
          setState(
            () {
              currentUserId = firebaseUser.uid;

              print("Logged in user id:- " + currentUserId);
            },
          );
          getUserType();
        }
      },
    );
  }

  @override
  dispose() {
    super.dispose();
    internetconnection.cancel();
    //cancel internent connection subscription after you are done
  }

  void getUserType() async {
    await Firestore.instance
        .collection('users')
        .document(currentUserId)
        .get()
        .then((DocumentSnapshot ds) {
      setState(() {
        loggedInUserType = ds.data['userType'];
      });
      // use ds as a snapshot
      print("User type:- " + loggedInUserType);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: <Color>[
                Color(0xffff9966),
                Color(0xffff5e62),
              ],
            ),
          ),
        ),
        title: Text(
          'Chat',
          style: GoogleFonts.firaCode(
              color: Color(0xff203152), fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          duration: Duration(milliseconds: 800),
          content: Text("Tap back again to exit"),
        ),
        child:Stack(
        children: <Widget>[
          // List
          Container(
            color: Colors.black12,
            child: StreamBuilder(
              stream: Firestore.instance.collection('users').snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                    ),
                  );
                } else {
                  return ListView.builder(
                    padding: EdgeInsets.all(10.0),
                    itemBuilder: (context, index) =>
                        buildItem(context, snapshot.data.documents[index]),
                    itemCount: snapshot.data.documents.length,
                  );
                }
              },
            ),
          ),
          // Loading
          Positioned(
            child: isLoading
                ? Container(
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.blue)),
                    ),
                    color: Colors.white.withOpacity(0.8),
                  )
                : Container(),
          )
        ],
      ),
    ), );
  }

  Widget buildItem(BuildContext context, DocumentSnapshot document) {
    if (loggedInUserType == "User") {
      if (document['id'] == currentUserId) {
        return Container();
      } else if (document['userType'] == "Admin") {
        return Container();
      } else if (document['userType'] == "User" &&
          document['isVerified'] == 'Not Verified') {
        return Container();
      } else {
        if (document['userType'] == "Doctor" &&
            document['isVerified'] == "Verified") {
          return InkWell(
            onTap: () {
              print(isoffline);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ChatScreen(
                            peerId: document.documentID,
                            peerAvatar: document['photoUrl'],
                          )));
              print(document.documentID);
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  colors: [
                    Color(0xffff00d2ff),
                    Color(0xffff928DAB),
                  ],
                ),
              ),
              padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
              margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
              child: Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Material(
                        child: document['photoUrl'] != null
                            ? CachedNetworkImage(
                                placeholder: (context, url) => Container(
                                  child: CircularProgressIndicator(
                                    strokeWidth: 1.0,
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.blue),
                                  ),
                                  width: 50.0,
                                  height: 50.0,
                                  padding: EdgeInsets.all(15.0),
                                ),
                                imageUrl: document['photoUrl'],
                                width: 50.0,
                                height: 50.0,
                                fit: BoxFit.cover,
                              )
                            : Icon(
                                Icons.account_circle,
                                size: 70.0,
                                color: Colors.grey,
                              ),
                        borderRadius: BorderRadius.all(Radius.circular(25.0)),
                        clipBehavior: Clip.hardEdge,
                      ),
                      Row(
                        children: [
                          Text(
                            document['userType'],
                            style: GoogleFonts.firaCode(),
                          ),
                          (document['isVerified'] == 'Verified')
                              ? Icon(
                                  Icons.check_circle_outline,
                                  size: 15,
                                  color: Colors.red,
                                )
                              : Container(),
                        ],
                      ),
                    ],
                  ),
                  Flexible(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Text(
                              document['fullName'] ?? 'Not available',
                              style: GoogleFonts.firaCode(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600),
                            ),
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                          ),
                          Container(
                            child: Text(
                              document['email'],
                              style: GoogleFonts.firaCode(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                            ),
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.only(left: 20.0),
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Container();
        }
      }
    } else if (loggedInUserType == "Doctor") {
      if (document['id'] == currentUserId) {
        return Container();
      } else if (document['userType'] == "Admin") {
        return Container();
      } else if (document['userType'] == "Doctor") {
        return Container();
      } else if (document['userType'] == "User") {
        return InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChatScreen(
                          peerId: document.documentID,
                          peerAvatar: document['photoUrl'],
                        )));
            print(document.documentID);
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                colors: [
                  Color(0xffff00d2ff),
                  Color(0xffff928DAB),
                ],
              ),
            ),
            padding: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 10.0),
            margin: EdgeInsets.only(bottom: 10.0, left: 5.0, right: 5.0),
            child: Row(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Material(
                      child: document['photoUrl'] != null
                          ? CachedNetworkImage(
                              placeholder: (context, url) => Container(
                                child: CircularProgressIndicator(
                                  strokeWidth: 1.0,
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.blue),
                                ),
                                width: 50.0,
                                height: 50.0,
                                padding: EdgeInsets.all(15.0),
                              ),
                              imageUrl: document['photoUrl'],
                              width: 50.0,
                              height: 50.0,
                              fit: BoxFit.cover,
                            )
                          : Icon(
                              Icons.account_circle,
                              size: 70.0,
                              color: Colors.grey,
                            ),
                      borderRadius: BorderRadius.all(Radius.circular(25.0)),
                      clipBehavior: Clip.hardEdge,
                    ),
                    Text(
                      document['userType'],
                      style: GoogleFonts.firaCode(),
                    ),
                  ],
                ),
                Flexible(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Text(
                            'Email: ${document['email']}',
                            style: GoogleFonts.firaCode(color: Colors.black),
                          ),
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 5.0),
                        ),
                        Container(
                          child: Text(
                            'Full Name: ${document['fullName'] ?? 'Not available'}',
                            style: GoogleFonts.firaCode(color: Colors.black),
                          ),
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                        ),
                      ],
                    ),
                    margin: EdgeInsets.only(left: 20.0),
                  ),
                ),
                Container(
                  child: (document['isVerified'] == 'Verified')
                      ? Icon(
                          Icons.verified_user,
                          color: Colors.green,
                        )
                      : Container(),
                ),
              ],
            ),
          ),
        );
      }
    } else {
      return LinearProgressIndicator();
    }
    return LinearProgressIndicator();
  }
}
