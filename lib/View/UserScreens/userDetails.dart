import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dogtor/View/otherScreens/PostScreens/fullPhoto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class UserDetail extends StatefulWidget {
  final String profileImage;
  final String fullName;
  final String email;
  final String userId;
  final String contact;
  final String verificationDocument;
  final String userType;
  final String address;
  final String isVerified;
  final String id;
  final String collectionName;

  UserDetail(
      {Key key,
      this.profileImage,
      this.fullName,
      this.email,
      this.userId,
      this.contact,
      this.verificationDocument,
      this.userType,
      this.address,
      this.isVerified,
      this.collectionName,
      this.id})
      : super(key: key);

  @override
  _UserDetailState createState() => _UserDetailState();
}

class _UserDetailState extends State<UserDetail> {
  final commentController = TextEditingController();
  final databaseReference = Firestore.instance;
  String userEmail;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    String status = widget.isVerified;

    String verificationDoc = widget.verificationDocument;
    void verifyUser() async {
      setState(() {
        isLoading = true;
      });
      await Firestore.instance
          .collection('users')
          .document(widget.userId)
          .updateData({'isVerified': "Verified"});
      setState(() {
        isLoading = false;
        Fluttertoast.showToast(
            msg: "User verified Successfully",
            textColor: Colors.red,
            backgroundColor: Colors.black);
      });
      Navigator.of(context).pushReplacementNamed('/AdminMainScreen');
    }

    if (widget.userType == "Doctor") {
      if (status == "Not Verified") {
        if (verificationDoc != "null") {
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(
                    Icons.chevron_left,
                    size: 30,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }),
              flexibleSpace: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
              ),
              centerTitle: true,
              title: Text("User Details"),
            ),
            backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Stack(
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.width,
                          width: MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              boxShadow: [
                                new BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.08),
                                  blurRadius: 20.0,
                                ),
                              ]),
                          padding:
                              EdgeInsets.only(top: 20, left: 12, right: 12),
                          child: Stack(
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: CachedNetworkImage(
                                  fit: BoxFit.cover,
                                  width: MediaQuery.of(context).size.width,
                                  placeholder: (context, url) =>
                                      CircularProgressIndicator(),
                                  imageUrl: widget.profileImage,
                                ),
                              ),
                              Align(
                                  alignment: Alignment.bottomRight,
                                  child: IconButton(
                                      icon: Image.asset(
                                          'lib/Assets/fullscreen.gif'),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => FullPhoto(
                                                      url: widget.profileImage,
                                                    )));
                                      })),
                            ],
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.all(12.0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0),
                                  boxShadow: [
                                    new BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.08),
                                      blurRadius: 20.0,
                                    ),
                                  ]),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Description of User Account: "
                                          .toUpperCase(),
                                      style: GoogleFonts.firaCode(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Divider(),
                                    Text(
                                      "Full Name: " + widget.fullName,
                                      style:
                                          GoogleFonts.firaCode(fontSize: 15.0),
                                      textAlign: TextAlign.justify,
                                    ),
                                    Divider(),
                                    Text(
                                      "Email: " + widget.email,
                                      style:
                                          GoogleFonts.firaCode(fontSize: 15.0),
                                      textAlign: TextAlign.justify,
                                    ),
                                    Divider(),
                                    Text(
                                      "Contact: " + widget.contact,
                                      style:
                                          GoogleFonts.firaCode(fontSize: 15.0),
                                      textAlign: TextAlign.justify,
                                    ),
                                    Divider(),
                                    Text(
                                      "User Type: " + widget.userType,
                                      style:
                                          GoogleFonts.firaCode(fontSize: 15.0),
                                      textAlign: TextAlign.justify,
                                    ),
                                    Divider(),
                                    Row(
                                      children: [
                                        Text(
                                          "verification Status: ",
                                          style: GoogleFonts.firaCode(
                                              fontSize: 15.0),
                                          textAlign: TextAlign.justify,
                                        ),
                                        (status == "Verified")
                                            ? Icon(Icons.check_circle)
                                            : FaIcon(
                                                FontAwesomeIcons.ellipsisH),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Text(
                              "verification Document",
                              style: GoogleFonts.firaCode(),
                            ),
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  boxShadow: [
                                    new BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.08),
                                      blurRadius: 20.0,
                                    ),
                                  ]),
                              margin:
                                  EdgeInsets.only(top: 20, left: 12, right: 12),
                              child: Stack(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(20),
                                    child: CachedNetworkImage(
                                      fit: BoxFit.cover,
                                      width: MediaQuery.of(context).size.width,
                                      placeholder: (context, url) =>
                                          CircularProgressIndicator(),
                                      imageUrl: widget.verificationDocument,
                                    ),
                                  ),
                                  Align(
                                      alignment: Alignment.bottomRight,
                                      child: IconButton(
                                          icon: Image.asset(
                                              'lib/Assets/fullscreen.gif'),
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        FullPhoto(
                                                          url: widget
                                                              .verificationDocument,
                                                        )));
                                          })),
                                ],
                              ),
                            ),
                          ],
                        ),
                        OfflineBuilder(
                          connectivityBuilder: (
                            BuildContext context,
                            ConnectivityResult connectivity,
                            Widget child,
                          ) {
                            bool connected =
                                connectivity != ConnectivityResult.none;
                            return connected
                                ? InkWell(
                                    onTap: verifyUser,
                                    child: Container(
                                      margin:
                                          EdgeInsets.only(top: 20, bottom: 20),
                                      width: MediaQuery.of(context).size.width /
                                          1.5,
                                      height: 50,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color: Colors.orange[400],
                                        border: Border.all(width: 1),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(10),
                                        ),
                                      ),
                                      child: Text(
                                        "Verify This Account".toUpperCase(),
                                        style: GoogleFonts.firaCode(),
                                      ),
                                    ),
                                  )
                                : Column(
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          border: Border.all(width: 2),
                                        ),
                                        alignment: Alignment.center,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                1.1,
                                        child: Column(
                                          children: [
                                            Image.asset(
                                                'lib/Assets/offline.gif'),
                                            Text(
                                              "OOPS, Looks Like your Internet is not working!",
                                              style: GoogleFonts.firaCode(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      )
                                    ],
                                  );
                          },
                          child: Container(),
                        ),
                      ],
                    ),
                  ),
                  isLoading
                      ? Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black.withOpacity(0.5),
                          child: Center(
                            child: CircularProgressIndicator(
                              strokeWidth: 1,
                              backgroundColor: Colors.redAccent,
                            ),
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          );
        } else {
          return Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(
                    Icons.chevron_left,
                    size: 30,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }),
              flexibleSpace: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
              ),
              centerTitle: true,
              title: Text(
                "User Details",
                style: GoogleFonts.firaCode(
                    color: Color(0xff203152), fontWeight: FontWeight.bold),
              ),
            ),
            backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.width,
                      width: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.08),
                              blurRadius: 20.0,
                            ),
                          ]),
                      padding: EdgeInsets.only(top: 20, left: 12, right: 12),
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              imageUrl: widget.profileImage,
                            ),
                          ),
                          Align(
                              alignment: Alignment.bottomRight,
                              child: IconButton(
                                  icon:
                                      Image.asset('lib/Assets/fullscreen.gif'),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => FullPhoto(
                                                  url: widget.profileImage,
                                                )));
                                  })),
                        ],
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.all(12.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: [
                                new BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.08),
                                  blurRadius: 20.0,
                                ),
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Description of User Account: ".toUpperCase(),
                                  style: GoogleFonts.firaCode(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                Divider(),
                                Text(
                                  "Full Name: " + widget.fullName,
                                  style: GoogleFonts.firaCode(fontSize: 15.0),
                                  textAlign: TextAlign.justify,
                                ),
                                Divider(),
                                Text(
                                  "Email: " + widget.email,
                                  style: GoogleFonts.firaCode(fontSize: 15.0),
                                  textAlign: TextAlign.justify,
                                ),
                                Divider(),
                                Text(
                                  "Contact: " + widget.contact,
                                  style: GoogleFonts.firaCode(fontSize: 15.0),
                                  textAlign: TextAlign.justify,
                                ),
                                Divider(),
                                Text(
                                  "User Type: " + widget.userType,
                                  style: GoogleFonts.firaCode(fontSize: 15.0),
                                  textAlign: TextAlign.justify,
                                ),
                                Divider(),
                                Row(
                                  children: [
                                    Text(
                                      "verification Status: ",
                                      style:
                                          GoogleFonts.firaCode(fontSize: 15.0),
                                      textAlign: TextAlign.justify,
                                    ),
                                    (status == "Verified")
                                        ? Icon(Icons.check_circle)
                                        : FaIcon(FontAwesomeIcons.ellipsisH),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "verification Document",
                          style: GoogleFonts.firaCode(),
                        ),
                        Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                boxShadow: [
                                  new BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.08),
                                    blurRadius: 20.0,
                                  ),
                                ]),
                            margin:
                                EdgeInsets.only(top: 20, left: 12, right: 12),
                            child: Text(
                              "The user has't uploaded verification document yet",
                              style: GoogleFonts.firaCode(),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      } else {
        return Scaffold(
          appBar: AppBar(
            leading: IconButton(
                icon: Icon(
                  Icons.chevron_left,
                  size: 30,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                }),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
            ),
            centerTitle: true,
            title: Text(
              "User Details",
              style: GoogleFonts.firaCode(
                  color: Color(0xff203152), fontWeight: FontWeight.bold),
            ),
          ),
          backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width,
                    width: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.08),
                            blurRadius: 20.0,
                          ),
                        ]),
                    padding: EdgeInsets.only(top: 20, left: 12, right: 12),
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: CachedNetworkImage(
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width,
                            placeholder: (context, url) =>
                                CircularProgressIndicator(),
                            imageUrl: widget.profileImage,
                          ),
                        ),
                        Align(
                            alignment: Alignment.bottomRight,
                            child: IconButton(
                                icon: Image.asset('lib/Assets/fullscreen.gif'),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => FullPhoto(
                                                url: widget.profileImage,
                                              )));
                                })),
                      ],
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              new BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.08),
                                blurRadius: 20.0,
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Description of User Account: ".toUpperCase(),
                                style: GoogleFonts.firaCode(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              Divider(),
                              Text(
                                "Full Name: " + widget.fullName,
                                style: GoogleFonts.firaCode(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                              Divider(),
                              Text(
                                "Email: " + widget.email,
                                style: GoogleFonts.firaCode(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                              Divider(),
                              Text(
                                "Contact: " + widget.contact,
                                style: GoogleFonts.firaCode(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                              Divider(),
                              Text(
                                "User Type: " + widget.userType,
                                style: GoogleFonts.firaCode(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                              Divider(),
                              Row(
                                children: [
                                  Text(
                                    "verification Status: ",
                                    style: GoogleFonts.firaCode(fontSize: 15.0),
                                    textAlign: TextAlign.justify,
                                  ),
                                  (status == "Verified")
                                      ? Icon(Icons.check_circle)
                                      : FaIcon(FontAwesomeIcons.ellipsisH),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        "verification Document",
                        style: GoogleFonts.firaCode(),
                      ),
                      Container(
                        height: 200,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              new BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.08),
                                blurRadius: 20.0,
                              ),
                            ]),
                        margin: EdgeInsets.only(
                            top: 20, left: 12, right: 12, bottom: 20),
                        child: Stack(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: CachedNetworkImage(
                                fit: BoxFit.cover,
                                width: MediaQuery.of(context).size.width,
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                imageUrl: widget.verificationDocument,
                              ),
                            ),
                            Align(
                                alignment: Alignment.bottomRight,
                                child: IconButton(
                                    icon: Image.asset(
                                        'lib/Assets/fullscreen.gif'),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => FullPhoto(
                                                    url: widget
                                                        .verificationDocument,
                                                  )));
                                    })),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      }
    } else {
      return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(
                Icons.chevron_left,
                size: 30,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              }),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
          ),
          centerTitle: true,
          title: Text(
            "User Details",
            style: GoogleFonts.firaCode(
                color: Color(0xff203152), fontWeight: FontWeight.bold),
          ),
        ),
        backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.width,
                  width: MediaQuery.of(context).size.height,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [
                        new BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.08),
                          blurRadius: 20.0,
                        ),
                      ]),
                  padding: EdgeInsets.only(top: 20, left: 12, right: 12),
                  child: Stack(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          imageUrl: widget.profileImage,
                        ),
                      ),
                      Align(
                          alignment: Alignment.bottomRight,
                          child: IconButton(
                              icon: Image.asset('lib/Assets/fullscreen.gif'),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FullPhoto(
                                              url: widget.profileImage,
                                            )));
                              })),
                    ],
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.08),
                              blurRadius: 20.0,
                            ),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Text(
                                "Description of User Account: ".toUpperCase(),
                                style: GoogleFonts.firaCode(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Divider(),
                            Text(
                              "Full Name:- " + widget.fullName,
                              style: GoogleFonts.firaCode(fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                            Divider(),
                            Text(
                              "Email:- " + widget.email,
                              style: GoogleFonts.firaCode(fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                            Divider(),
                            Text(
                              "Contact:- " + widget.contact,
                              style: GoogleFonts.firaCode(fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
