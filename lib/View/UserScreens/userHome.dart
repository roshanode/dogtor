import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dogtor/View/DoctorScreens/doctorDetails.dart';
import 'package:dogtor/View/otherScreens/BreedsScreens/allBreeds.dart';
import 'package:dogtor/View/otherScreens/BreedsScreens/dogsBreedDetails.dart';
import 'package:dogtor/View/otherScreens/DiseaseScreens/allDiseases.dart';
import 'package:dogtor/View/otherScreens/DiseaseScreens/diseaseDetail.dart';
import 'package:dogtor/View/otherScreens/VaccineScreens/vaccine.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

class UserHomeScreen extends StatefulWidget {
  @override
  _UserHomeScreenState createState() => _UserHomeScreenState();
}

class _UserHomeScreenState extends State<UserHomeScreen> {
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  String userId;

  @override
  void initState() {
    super.initState();

    getUserData();
  }

  void getUserData() async {
    await FirebaseAuth.instance.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        setState(() {
          userId = firebaseUser.uid;
          print("Logged in user ID is:- " + userId);
        });
      }
    });
    await registerNotification();
    await configLocalNotification();
  }

  Future<Null> registerNotification() {
    firebaseMessaging.requestNotificationPermissions();

    firebaseMessaging.configure(onMessage: (Map<String, dynamic> message) {
      print('onMessage: $message');
      Platform.isAndroid
          ? showNotification(message['notification'])
          : showNotification(message['aps']['alert']);
      return;
    }, onResume: (Map<String, dynamic> message) {
      print('onResume: $message');
      Platform.isAndroid
          ? showNotification(message['notification'])
          : showNotification(message['aps']['alert']);
      return;
    }, onLaunch: (Map<String, dynamic> message) {
      print('onLaunch: $message');
      Platform.isAndroid
          ? showNotification(message['notification'])
          : showNotification(message['aps']['alert']);
      return;
    });

    firebaseMessaging.getToken().then((token) {
      print('token: $token');
      Firestore.instance
          .collection('users')
          .document(userId)
          .updateData({'pushToken': token});
    }).catchError((err) {
      Fluttertoast.showToast(msg: err.message.toString());
    });
    return null;
  }

  Future<Null> configLocalNotification() {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);
    return null;
  }

  void showNotification(message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'com.dogtor.dogtor',
      'Dogtor',
      'Dogtor Platform to connect Dog owner with Doctors',
      playSound: true,
      enableVibration: true,
      importance: Importance.Max,
      priority: Priority.High,
    );
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    print(message);
    await flutterLocalNotificationsPlugin.show(0, message['title'].toString(),
        message['body'].toString(), platformChannelSpecifics,
        payload: json.encode(message));
  }

  void signOut() async {
    try {
      await FirebaseAuth.instance.signOut();
      Navigator.of(context).pushReplacementNamed('/Splash');
    } catch (e) {
      print(e);
    }
  }

  void _exitAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                    colors: [Color(0xffffff7e5f), Color(0xfffffeb47b)])),
            height: 100,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Expanded(
                  child: Text("Are You Sure ?",
                      style: GoogleFonts.firaCode(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.w800)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        IconButton(
                          icon: FaIcon(
                            FontAwesomeIcons.timesCircle,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        Text(
                          "No".toUpperCase(),
                          style: GoogleFonts.firaCode(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: FaIcon(
                            FontAwesomeIcons.signOutAlt,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            signOut();
                            Navigator.of(context).pop();
                          },
                        ),
                        Text(
                          "Yes".toUpperCase(),
                          style: GoogleFonts.firaCode(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildListItemBreeds(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DogsBreedDetails(
              breed: document['Breed'],
              description: document['Description'],
              height: document['Height'],
              weight: document['Weight'],
              lifeSpan: document['Life Span'],
              pictures: document['Pictures'],
              id: document.documentID,
              collectionName: collectionName,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.all(8),
        width: MediaQuery.of(context).size.width / 1.5,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              new BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.5),
                blurRadius: 20.0,
              ),
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                    imageUrl: document['Pictures'][0],
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              alignment: Alignment.center,
              child: Text(
                document['Breed'],
                style: GoogleFonts.firaCode(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListItemDiseases(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DiseaseDetails(
              diseaseName: document['Disease Name'],
              diseaseDetail: document['Disease Detail'],
              symptoms: document['Symptoms'],
              precautions: document['Precautions'],
              pictures: document['Pictures'],
              id: document.documentID,
              collectionName: collectionName,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.all(8),
        width: MediaQuery.of(context).size.width / 1.5,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              new BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.5),
                blurRadius: 20.0,
              ),
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                    imageUrl: document['Pictures'][0],
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              child: Text(
                document['Disease Name'],
                style: GoogleFonts.firaCode(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListItemDoctors(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DoctorDetails(
                      profileImage: document['photoUrl'],
                      fullName: document['fullName'],
                      email: document['email'],
                      userId: document['id'],
                      contact: document['contact'],
                      userType: document['userType'],
                      address: document['address'],
                      isVerified: document["isVerified"],
                      id: document.documentID,
                      collectionName: collectionName,
                    )),
          );
        },
        child: Container(
          margin: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 10),
          width: MediaQuery.of(context).size.width / 1.5,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [
                new BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.5),
                  blurRadius: 20.0,
                ),
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: CachedNetworkImage(
                      imageUrl: document['photoUrl'],
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 4,
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: [
                        Text(
                          document['fullName'],
                          overflow: TextOverflow.fade,
                          style: GoogleFonts.firaCode(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        (document['isVerified'] == "Verified")
                            ? Icon(
                                Icons.check_circle_outline,
                                size: 15,
                              )
                            : Container(),
                      ],
                    ),
                    Text(
                      document['email'],
                      overflow: TextOverflow.fade,
                      style: GoogleFonts.firaCode(color: Colors.black),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "HOME",
          style: GoogleFonts.firaCode(
              color: Color(0xff203152),
              fontWeight: FontWeight.bold,
              fontSize: 20),
        ),
        leading: StreamBuilder(
            stream: Firestore.instance
                .collection('users')
                .document(userId)
                .snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return LinearProgressIndicator(
                  backgroundColor: Colors.green,
                );
              return Container(
                width: 100,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                        child: ClipRRect(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                      ),
                      child: CachedNetworkImage(
                        imageUrl: snapshot.data.data['photoUrl'],
                        height: 50,
                        width: 50,
                        fit: BoxFit.cover,
                      ),
                    )),
                  ],
                ),
              );
            }),
        elevation: 30,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
        ),
        //
        actions: <Widget>[
          Center(
              child: Text(
            "Log Out".toUpperCase(),
            style: GoogleFonts.firaCode(),
          )),
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () => _exitAlert(),
          ),
        ],
      ),
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          duration: Duration(milliseconds: 800),
          content: Text("Tap back again to exit"),
        ),
        child: Container(
          color: Colors.black12,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(8),
                      child: Text(
                        'Dogs Breeds',
                        style: GoogleFonts.firaCode(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AllBreeds()));
                      },
                      child: Container(
                        margin: EdgeInsets.all(8),
                        child: Text(
                          'See More 👉',
                          style: GoogleFonts.firaCode(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              fontStyle: FontStyle.italic,
                              color: Colors.blue),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 2.5,
                  child: StreamBuilder(
                      stream: Firestore.instance
                          .collection('Dogs Breeds')
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return LinearProgressIndicator(
                            backgroundColor: Colors.green,
                          );
                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (context, index) => _buildListItemBreeds(
                              context,
                              snapshot.data.documents[index],
                              'Dogs Breeds'),
                        );
                      }),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.all(8),
                      child: Text(
                        'Diseases',
                        style: GoogleFonts.firaCode(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AllDiseases()));
                      },
                      child: Container(
                        margin: EdgeInsets.all(8),
                        child: Text(
                          'See More 👉',
                          style: GoogleFonts.firaCode(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              fontStyle: FontStyle.italic,
                              color: Colors.blue),
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 2.5,
                  child: StreamBuilder(
                      stream:
                          Firestore.instance.collection('diseases').snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return LinearProgressIndicator(
                            backgroundColor: Colors.green,
                          );
                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: (snapshot.data.documents.length >= 4)
                              ? snapshot.data.documents.length - 4
                              : snapshot.data.documents,
                          itemBuilder: (context, index) =>
                              _buildListItemDiseases(context,
                                  snapshot.data.documents[index], 'diseases'),
                        );
                      }),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Vaccine()));
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    width: MediaQuery.of(context).size.width,
                    height: 100,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xffffFFEFBA), Color(0xffffFFFFFF)]),
                      border: Border.all(width: 0.1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Text(
                            "Also, Want to Know About Vaccination?",
                            style: GoogleFonts.firaCode(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Expanded(
                          flex: 0,
                          child: Text(
                            "Click Here",
                            style: GoogleFonts.firaCode(
                                fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                        Expanded(
                            flex: 1, child: Image.asset("lib/Assets/tap.gif")),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20),
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    "Doctors",
                    style: GoogleFonts.firaCode(
                        fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height / 2.5,
                  width: MediaQuery.of(context).size.width,
                  child: StreamBuilder(
                    stream: Firestore.instance.collection('users').snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return LinearProgressIndicator(
                          backgroundColor: Colors.green,
                        );
                      } else {
                        var users = [];
                        for (int i = 0;
                            i < snapshot.data.documents.length;
                            i++) {
                          if (snapshot.data.documents[i]["userType"] ==
                              "Doctor") {
                            if (snapshot.data.documents[i]["isVerified"] ==
                                "Verified") {
                              users.add(snapshot.data.documents[i]);
                            }
                          }
                        }
                        return ListView.builder(
                          // itemExtent: 200,
                          scrollDirection: Axis.horizontal,
                          itemCount: users.length,
                          itemBuilder: (context, index) =>
                              _buildListItemDoctors(
                                  context, users[index], 'users'),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
