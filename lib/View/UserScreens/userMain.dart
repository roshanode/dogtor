import 'package:dogtor/View/ChatScreens/allUsers.dart';
import 'package:dogtor/View/UserScreens/userHome.dart';
import 'package:dogtor/View/UserScreens/userProfile.dart';
import 'package:dogtor/View/otherScreens/PostScreens/feeds.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

class UserMainScreen extends StatefulWidget {
  @override
  _UserMainScreenState createState() => _UserMainScreenState();
}

class _UserMainScreenState extends State<UserMainScreen> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    final List<Widget> _widgetOptions = <Widget>[
      UserHomeScreen(),
      Feeds(),
      AllUsers(),
      UserProfile(),
    ];
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
            color: Colors.white54,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(blurRadius: 10, color: Colors.black.withOpacity(.5))
            ]),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 8),
            child: GNav(
                gap: 5,
                activeColor: Colors.white,
                iconSize: 20,
                textStyle: GoogleFonts.firaCode(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.white),
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                duration: Duration(milliseconds: 400),
                tabBackgroundColor: Colors.orange,
                tabs: [
                  GButton(
                    icon: FontAwesomeIcons.home,
                    text: 'Home',
                  ),
                  GButton(
                    icon: FontAwesomeIcons.newspaper,
                    text: 'Posts',
                  ),
                  GButton(
                    icon: FontAwesomeIcons.comments,
                    text: 'Chat',
                  ),
                  GButton(
                    icon: FontAwesomeIcons.user,
                    text: 'Profile',
                  ),
                ],
                selectedIndex: _selectedIndex,
                onTabChange: (index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                }),
          ),
        ),
      ),
    );
  }
}
