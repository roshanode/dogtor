import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dogtor/View/otherScreens/DiseaseScreens/diseaseDetail.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';

class AllDiseases extends StatefulWidget {
  @override
  _AllDiseasesState createState() => _AllDiseasesState();
}

class _AllDiseasesState extends State<AllDiseases> {
  String userId;
  String loggedInUserType;
  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        setState(() {
          userId = firebaseUser.uid;
          print("Logged in user ID:- " + userId);
        });
        getUserType();
      }
    });
  }

  void getUserType() async {
    await Firestore.instance
        .collection('users')
        .document(userId)
        .get()
        .then((DocumentSnapshot ds) {
      setState(() {
        loggedInUserType = ds.data['userType'];
      });
      print("User type:- " + loggedInUserType);
    });
  }

  Widget _buildListItemPosts(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DiseaseDetails(
              diseaseName: document['Disease Name'],
              diseaseDetail: document['Disease Detail'],
              symptoms: document['Symptoms'],
              precautions: document['Precautions'],
              pictures: document['Pictures'],
              id: document.documentID,
              collectionName: collectionName,
            ),
          ),
        );
      },
      child: Container(
        height: MediaQuery.of(context).size.height / 2.5,
        margin: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 10),
        width: MediaQuery.of(context).size.width / 1.6,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              new BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.5),
                blurRadius: 20.0,
              ),
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: CachedNetworkImage(
                    imageUrl: document['Pictures'][0],
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    document['Disease Name'],
                    overflow: TextOverflow.fade,
                    style: GoogleFonts.firaCode(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  Expanded(
                    child: StreamBuilder(
                        stream: Firestore.instance
                            .collection('users')
                            .document(userId)
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return LinearProgressIndicator(
                              backgroundColor: Colors.green,
                            );
                          } else if (snapshot.data.data == null) {
                            return CircularProgressIndicator(
                              backgroundColor: Colors.green,
                            );
                          } else if (snapshot.data.data['userType'] ==
                              "Admin") {
                            return InkWell(
                              onTap: () async {
                                await Firestore.instance
                                    .collection('diseases')
                                    .document(document.documentID)
                                    .delete();
                                for (var i = 0;
                                    i < document['Pictures'].length;
                                    i++) {
                                  await FirebaseStorage.instance
                                      .getReferenceFromUrl(
                                          document['Pictures'][i])
                                      .then((value) => value.delete())
                                      .catchError((onError) {
                                    print(onError);
                                  });
                                }

                                Fluttertoast.showToast(
                                    msg: "Deleted Successfully",
                                    toastLength: Toast.LENGTH_LONG,
                                    backgroundColor: Colors.green,
                                    textColor: Colors.white);
                              },
                              child: Container(
                                  child: Image.asset(
                                'lib/Assets/delete.png',
                                height: 30,
                              )),
                            );
                          } else {
                            return Container();
                          }
                        }),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "All Diseases",
          style: GoogleFonts.firaCode(
              color: Color(0xff203152), fontWeight: FontWeight.bold),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
        ),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 30,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: StreamBuilder(
                  stream: Firestore.instance.collection('diseases').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    return ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) => _buildListItemPosts(
                          context, snapshot.data.documents[index], 'diseases'),
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
}
