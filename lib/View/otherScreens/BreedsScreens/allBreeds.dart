import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dogtor/View/otherScreens/BreedsScreens/dogsBreedDetails.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';

class AllBreeds extends StatefulWidget {
  @override
  _AllBreedsState createState() => _AllBreedsState();
}

class _AllBreedsState extends State<AllBreeds> {
  String userId;
  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        setState(() {
          userId = firebaseUser.uid;
          print("Logged in user ID:- " + userId);
        });
      }
    });
  }

  Widget _buildListItemPosts(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DogsBreedDetails(
                breed: document['Breed'],
                description: document['Description'],
                height: document['Height'],
                weight: document['Weight'],
                lifeSpan: document['Life Span'],
                pictures: document['Pictures'],
                id: document.documentID,
                collectionName: collectionName,
              ),
            ),
          );
        },
        child: Container(
          height: MediaQuery.of(context).size.height / 2.5,
          margin: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 10),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [
                new BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.5),
                  blurRadius: 20.0,
                ),
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: CachedNetworkImage(
                      imageUrl: document['Pictures'][0],
                      width: MediaQuery.of(context).size.width,
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(left: 10, top: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      document['Breed'],
                      overflow: TextOverflow.fade,
                      style: GoogleFonts.firaCode(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    Expanded(
                      child: StreamBuilder(
                          stream: Firestore.instance
                              .collection('users')
                              .document(userId)
                              .snapshots(),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return LinearProgressIndicator(
                                backgroundColor: Colors.green,
                              );
                            } else if (snapshot.data.data == null) {
                              return CircularProgressIndicator(
                                backgroundColor: Colors.green,
                              );
                            } else if (snapshot.data.data['userType'] ==
                                "Admin") {
                              return InkWell(
                                onTap: () async {
                                  await Firestore.instance
                                      .collection('Dogs Breeds')
                                      .document(document.documentID)
                                      .delete();
                                  for (var i = 0;
                                      i < document['Pictures'].length;
                                      i++) {
                                    await FirebaseStorage.instance
                                        .getReferenceFromUrl(
                                            document['Pictures'][i])
                                        .then((value) => value.delete())
                                        .catchError((onError) {
                                      print(onError);
                                    });
                                  }

                                  Fluttertoast.showToast(
                                      msg: "Deleted Successfully",
                                      toastLength: Toast.LENGTH_LONG,
                                      backgroundColor: Colors.green,
                                      textColor: Colors.white);
                                },
                                child: Container(
                                    child: Image.asset(
                                  'lib/Assets/delete.png',
                                  height: 30,
                                )),
                              );
                            } else {
                              return Container();
                            }
                          }),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "All breeds",
          style: GoogleFonts.firaCode(
              color: Color(0xff203152), fontWeight: FontWeight.bold),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Color(0xffff9966), Color(0xffff5e62)])),
        ),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 30,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: StreamBuilder(
                  stream:
                      Firestore.instance.collection('Dogs Breeds').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    return ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) => _buildListItemPosts(
                          context,
                          snapshot.data.documents[index],
                          'Dogs Breeds'),
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
}
